package com.example.demo;

public class Sample {
    private String val;

    public Sample(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
