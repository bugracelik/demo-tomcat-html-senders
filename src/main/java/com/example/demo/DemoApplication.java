package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {


    public static void main(String[] args) {
        System.setProperty("server.port", "8001");
        SpringApplication.run(DemoApplication.class, args);
    }


    @GetMapping("sample")
    public Sample baba()
    {
        Sample s = new Sample("121");

        s.setVal("212");

        String s1 = "<h1>sda</h1>";

        // if(s.equals("212")) http.get();


        return new Sample("bugra");
    }


    // http response
    @GetMapping("")
    public String index()
    {
        return "        <!DOCTYPE html>\n" +
                "        <html>\n" +
                "        <head>\n" +
                "        <title>Page Title</title>\n" +
                "\n" +
                "\n" +
                "        <style>\n" +
                "\n" +
                "            #bugra1 {\n" +
                "                background-color: purple;\n" +
                "            }\n" +
                "\n" +
                "            h1 {\n" +
                "                background-color: red;\n" +
                "                color: black;\n" +
                "                text-align: center;\n" +
                "            }\n" +
                "\n" +
                "            body {\n" +
                "                background-color: lightblue;\n" +
                "            }\n" +
                "\n" +
                "            p {\n" +
                "                font-family: verdana;\n" +
                "                font-size: 20px;\n" +
                "            }\n" +
                "        \n" +
                "        \n" +
                "        </style>\n" +
                "\n" +
                "\n" +
                "        </head>\n" +
                "        <body>\n" +
                "\n" +
                "        <h1 id=\"bugra1\">inner html</h1>\n" +
                "        <h1 id=\"bugra2\">This is a Heading</h1>\n" +
                "        <h1 id=\"bugra3\">This is a Heading</h1>\n" +
                "        <h1 id=\"bugra4\">This is a Heading</h1>\n" +
                "\n" +
                "        <p style=\"text-align: center;\">asdadsa</p>\n" +
                "\n" +
                "        <button id= \"enter\">tıkla</button>\n" +
                "\n" +
                "        <button id= \"enter1\">tıkla-bugra</button>\n" +
                "\n" +
                "\n" +
                "        <button id= \"github\">github-api-call</button>\n" +
                "        <button id= \"clear\">clear</button>\n" +
                "\n" +
                "\n" +
                "        <h1 id=\"github-response\">buraya response gelebilme ihtimali var</h1>\n" +
                "\n" +
                "        <p>This is a paragraph.</p>\n" +
                "\n" +
                "\n" +
                "        <script>\n" +
                "            \n" +
                "            console.log(2 + 2)\n" +
                "\n" +
                "            var a = \"bugraasdadad\"\n" +
                "\n" +
                "            console.log(a)\n" +
                "\n" +
                "            alert(2 + 2)\n" +
                "\n" +
                "            var element = document.getElementById(\"bugra1\")\n" +
                "            var str = element.innerHTML\n" +
                "            console.log(str)\n" +
                "\n" +
                "            var button = document.getElementById(\"enter\")\n" +
                "            button.onclick = function () { \n" +
                "                \n" +
                "                alert('hello!'); \n" +
                "                console.log(\"enter clicked\")\n" +
                "                http.get();\n" +
                "\n" +
                "            };\n" +
                "\n" +
                "\n" +
                "            function bugra()\n" +
                "            {\n" +
                "                alert(\"java script\")\n" +
                "            }\n" +
                "            \n" +
                "            \n" +
                "            </script>\n" +
                "\n" +
                "            <script>\n" +
                "                var buttonGithub = document.getElementById(\"github\")\n" +
                "\n" +
                "                var buttonClear = document.getElementById(\"clear\")\n" +
                "\n" +
                "                buttonClear.onclick = function() {\n" +
                "                    document.getElementById(\"github-response\").innerHTML = \"\"\n" +
                "                }\n" +
                "\n" +
                "                buttonGithub.onclick = function () { \n" +
                "                \n" +
                "                \n" +
                "                console.log(\"github\")\n" +
                "\n" +
                "\n" +
                "                var xmlHttp = new XMLHttpRequest();\n" +
                "                xmlHttp.open( \"GET\", \"https://api.github.com/users/tcelik\", false ); // false for synchronous request\n" +
                "                xmlHttp.send( null );\n" +
                "                var response = xmlHttp.response\n" +
                "                console.log(xmlHttp.response)\n" +
                "\n" +
                "                document.getElementById(\"github-response\").innerHTML = response\n" +
                "\n" +
                "                \n" +
                "            };\n" +
                "            </script>\n" +
                "\n" +
                "        </body>\n" +
                "        </html>\n";
    }

}
